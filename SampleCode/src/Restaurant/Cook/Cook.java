/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Restaurant.Cook;

/**
 *
 * @author mazu
 */
public class Cook {
    
    private String name;
    protected static String cookMsg="を調理した!!";
    
    public Cook(String name){
        this.name = name;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void cookFood(){
        System.out.println("何でも調理するよ。");
    }
}
