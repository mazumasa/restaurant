/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Restaurant.Cook;

/**
 *
 * @author mazu
 */
public class WesternCook extends Cook{
    public WesternCook(String name){
        super(name);
    }
    
    @Override
    public void cookFood(){
        this.cookHamburger();
    }
    
    private void cookHamburger(){
        System.out.println("ハンバーグ"+this.cookMsg);
    }
}
