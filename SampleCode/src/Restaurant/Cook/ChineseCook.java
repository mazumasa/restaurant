/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Restaurant.Cook;

/**
 *
 * @author mazu
 */
public class ChineseCook extends Cook {
    
    public ChineseCook(String name){
        super(name);
    }
    
    @Override
    public void cookFood(){
        this.cookCharhan();
    }
    
    private void cookCharhan(){
        System.out.println("チャーハン"+this.cookMsg);
    }
}
