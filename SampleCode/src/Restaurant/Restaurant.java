/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Restaurant;

import Restaurant.Cook.Cook;
import Restaurant.Cook.ChineseCook;
import Restaurant.Cook.JapaneseCook;
import Restaurant.Cook.WesternCook;




/**
 *
 * @author mazu
 */
public class Restaurant {
    private Cook[] cooks;
    private final String cookMsg = "の調理！！";
    
    public Restaurant(){
        this.cooks = new Cook[]{new Cook("Yamada"),new ChineseCook("Tanaka"),
                                new JapaneseCook("Satou"),new WesternCook("Ishikawa")};
    }
    
    public void open(){
        for(Cook cook : cooks){
            System.out.println(cook.getName()+this.cookMsg);
            cook.cookFood();
        }
    }
}
