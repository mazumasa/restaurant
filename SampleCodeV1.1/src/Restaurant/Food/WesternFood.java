/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Restaurant.Food;

/**
 *
 * @author mazu
 */
public enum WesternFood implements Ifood{
    HAMBURGER("ハンバーグ",30,35);
    
    private String name;
    private int cost;
    private int price;
    WesternFood(String name,int cost,int price){
        this.name = name;
        this.cost = cost;
        this.price = price;
    }
    public String getName(){
        return this.name;
    }
    public int getCost(){
        return this.cost;
    }
    public int getPrice(){
        return this.price;
    }
}
