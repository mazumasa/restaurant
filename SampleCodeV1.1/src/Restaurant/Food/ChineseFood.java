/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Restaurant.Food;

/**
 *
 * @author mazu
 */
public enum ChineseFood implements Ifood {
    CHARHAN("炒飯",20,25);
    
    private String name;
    private int cost;
    private int price;
    ChineseFood(String name,int cost,int price){
        this.name = name;
        this.cost = cost;
        this.price = price;
    }
    @Override
    public String getName(){
        return this.name;
    }
    @Override
    public int getCost(){
        return this.cost;
    }
    @Override
    public int getPrice(){
        return this.price;
    }
}
