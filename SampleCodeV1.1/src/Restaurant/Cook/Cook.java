/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Restaurant.Cook;

import Restaurant.Food.*;
/**
 *
 * @author mazu
 */
public class Cook {
    
    private String name;
    private Ifood foodType;
    protected final String cookMsg="を調理した!!";
    protected final String noFoodMsg="は材料不足で調理できません!!";
    
    public Cook(String name, Ifood foodType){
        this.name = name;
        this.foodType = foodType;
    }
    
    public String getName(){
        return this.name;
    }
    
    public Ifood cookFood(int resource){
        if(this.isCookFood(resource) == false){
            System.out.println(this.foodType.getName()+this.noFoodMsg);
            return null;
        }
        System.out.println(this.foodType.getName()+this.cookMsg);
        return this.foodType;
    }
    
    private boolean isCookFood(int resource){
        return resource > this.foodType.getCost();
    }
}
