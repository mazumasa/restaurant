/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Restaurant;

import Restaurant.Cook.*;
import Restaurant.Food.*;



/**
 *
 * @author mazu
 */
public class Restaurant {
    private Cook[] cooks;
    private int money;
    private int resource;
    private final String cookMsg = "の調理！！";
    
    public Restaurant(int resource,int money){
        this.resource = resource;
        this.money = money;
        
        this.cooks = new Cook[]{new Cook("Tanaka",WesternFood.HAMBURGER),new Cook("Saitou",JapaneseFood.SASHIMI),
                                new Cook("Ishikawa",ChineseFood.CHARHAN)};
    }
    
    public void open(){
        this.report();        
        for(Cook cook : cooks){
            System.out.println(cook.getName()+this.cookMsg);
            Ifood result = cook.cookFood(this.resource);
            this.manage(result);
            this.report();
        }       
    }
    private void report(){
        System.out.println("材料："+this.resource+" 資金："+this.money);
    }
    private void manage(Ifood result){
        if(result != null){
            this.money = this.money + result.getPrice();
            this.resource = this.resource - result.getCost();
        }
    }
}
